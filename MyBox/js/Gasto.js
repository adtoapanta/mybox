﻿$(document).ready(function () { initGasto(); });

function initGasto() {
    console.log('init gasto');

    $('#btnGuardarGasto').click(function (e) {
        guardarOEditar();

    });

    $('#btnConsultarGastos').click(function (e) {
        consultarGastos();

    });


    $('#modalGastos').on('hidden.bs.modal', function () {
        limpiarCampos();
    });

    exportTable();
    //consultarProductos();
}
var table;

function exportTable() {

    table = $('#tblGastos').DataTable({
        lengthChange: false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        language: SPANISHDATATABLE,

    });

}

function guardarOEditar() {

    var id = $("#txtIdGasto").val();
    if (id == 0)
        guardarGasto();
    else
        editarGasto();

}

function limpiarCampos() {

    $("#txtIdGasto").val(0);
    $("#txtProveedor").val('');
    $("#txtConcepto").val('');
    $("#txtValor").val('');
    $("#txtFactura").val('');

}

function guardarGasto() {

    var proveedor = $("#txtProveedor").val();
    var concepto = $("#txtConcepto").val();
    var valor = $("#txtValor").val();
    var factura = $("#txtFactura").val();

    htclibjs.Ajax({
        url: "/Gasto/guardarGasto",
        data: JSON.stringify({
            proveedor: proveedor,
            concepto: concepto,
            valor: valor,
            factura: factura
        }),
        success: function (r) {

            toastr.success("Gasto Guardado", { positionClass: "toast-top-center" });
            $("#modalGastos").modal('hide');
            consultarGastos();
        }
    });

}

function editarGasto() {

    var id = $("#txtIdGasto").val();
    var proveedor = $("#txtProveedor").val();
    var concepto = $("#txtConcepto").val();
    var valor = $("#txtValor").val();
    var factura = $("#txtFactura").val();

    htclibjs.Ajax({
        url: "/Gasto/editarGasto",
        data: JSON.stringify({
            idGasto: id,
            proveedor: proveedor,
            concepto: concepto,
            valor: valor,
            factura: factura
        }),
        success: function (r) {

            toastr.success("Gasto Actualizado", { positionClass: "toast-top-center" });
            $("#modalGastos").modal('hide');
            consultarGastos();

        }
    });

}

function consultarGastos() {

    var desde = $("#txtDesde").val();
    var hasta = $("#txtHasta").val();

    table.destroy();

    htclibjs.Ajax({
        url: "/Gasto/consultarGastos",
        data: JSON.stringify({
            desde: desde,
            hasta: hasta
        }),
        success: function (r) {

            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th></th>' + //fila vacia donde va el boton de editar
                '<th>Proveedor</th>' +
                '<th>Concepto</th>' +
                '<th>Fecha</th>' +
                '<th>Factura</th>' +
                '<th>Valor</th>' +
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                var fecha = item.fecha.split('T');

                trHTML +=

                    '<tr>' +
                    //Menu opciones de la tabla
                    '<td id="tdIdPersona' + item.id + '"> ' +
                    '<div class="dropdown">' +
                    '<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                    '<i class="fas fa-cog"></i>' +
                    '</button>' +
                    '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +
                    '<a id ="' + item.id + '" class="dropdown-item btnSeleccionarGasto" href="#" data-toggle="modal" data-target="#modalGastos" >Editar</a>' +

                    '</div>' +
                    '</div>' +
                    '</td > ' +
                    '<td >' + item.proveedor + '</td>' +
                    '<td >' + item.concepto + '</td>' +
                    '<td >' + fecha[0] + '</td>' +
                    '<td >' + item.factura + '</td>' +
                    '<td >' + item.valor + '</td>' +
                    //'<td id="tdIdPersona' + item.id + '"> <button class="btnSeleccionarPersona btn btn-link  " value="' + item.id + '" type="button" data-toggle="modal" data-target="#modalAtletas" >Editar</button> </td>' + //boton editar de la tabla
                    '</tr> ';


            });

            trHTML +=
                '</tbody>';

            $('#tblGastos').html('');
            $('#tblGastos').append(trHTML);

            //accion al hacer clic en boton de la tabla
            $('.btnSeleccionarGasto').on('click', function (e) {

                console.log('clic');
                selecionarGasto($(this));

            });

            exportTable();

        }
    });

}

function selecionarGasto(boton) {

    var id = boton.attr("id");

    htclibjs.Ajax({
        url: "/Gasto/consultarGasto",
        data: JSON.stringify({
            idGasto: id
        }),
        success: function (r) {

            var item = r.data;

            $("#txtIdGasto").val(item.id);
            $("#txtProveedor").val(item.proveedor);
            $("#txtConcepto").val(item.concepto);
            $("#txtValor").val(item.valor);
            $("#txtFactura").val(item.factura);

        }
    });

}