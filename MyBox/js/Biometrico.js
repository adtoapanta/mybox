﻿$(document).ready(function () { initBiometrico(); });

function initBiometrico() {
    console.log('init Biometrico');

    $('#btnConsultarBiometrico').click(function (e) {
        consultarBiometrico();
        consultarSaldosPendientes();
    });

    $('#btnGuardarMembresia').click(function (e) {
        editarMembresia();
    });    

    var idPersona = $('#txtidPersona').val();
    if (idPersona != '') {
        consultarBiometrico(idPersona);
        consultarSaldosPendientes(idPersona);
    }
    
}

function editarMembresia() {

    var idMembresia = $('#txtidMembresia').val();
    var dias = $('#txtDiasAgregar').val();
    var factura = $('#txtFacturaMembresia').val();

    htclibjs.Ajax({
        url: "/Membresia/editarMembresia",
        data: JSON.stringify({
            idMembresia: idMembresia,
            diasExtra: dias,
            factura: factura
        }),

        success: function (r) {

            toastr.success("Membresia Actualizada", { positionClass: "toast-top-center" });
            $('#modalEditarMembresia').modal('hide');
            consultarBiometrico();
        }
        
    });

}



function consultarBiometrico() {

    var documento = $('#txtDocumentoBiometrico').val();

    htclibjs.Ajax({
        url: "/Biometrico/consultarBiometrico",
        data: JSON.stringify({
            idPersona: documento
        }),

        success: function (r) {

            var persona = r.data;
            var membresia = persona.Membresias[persona.Membresias.length - 1];
            var fecha = membresia.fin.split('T');
            var desde = membresia.inicio.split('T');
            var nombreCompleto = persona.nombre1 + ' ' + persona.nombre2 + ' ' + persona.apellido1 + ' ' + persona.apellido2;
            $('#txtidMembresia').val(membresia.id);
            $('#atletaBiometrico').html(nombreCompleto);
            $('#estadoBiometrico').html(membresia.activo);
            $('#validoHastaBiometrico').html(fecha[0]);
            $('#diasRestantesBiometrico').html(membresia.diasRestantes);

            $('#txtFacturaMembresia').val(membresia.factura);
            $('#txtDesdeMembresia').val(desde[0]);
            $('#txtHastaMembresia').val(fecha[0]);

        }

    });

}

function consultarBiometrico(idPersona) {

    

    htclibjs.Ajax({
        url: "/Biometrico/consultarBiometrico",
        data: JSON.stringify({
            idPersona: idPersona
        }),

        success: function (r) {

            var persona = r.data;
            var membresia = persona.Membresias[persona.Membresias.length - 1];
            var fecha = membresia.fin.split('T');
            var desde = membresia.inicio.split('T');
            var nombreCompleto = persona.nombre1 + ' ' + persona.nombre2 + ' ' + persona.apellido1 + ' ' + persona.apellido2;
            $('#txtidMembresia').val(membresia.id);
            $('#atletaBiometrico').html(nombreCompleto);
            $('#estadoBiometrico').html(membresia.activo);
            $('#validoHastaBiometrico').html(fecha[0]);
            $('#diasRestantesBiometrico').html(membresia.diasRestantes);

            $('#txtFacturaMembresia').val(membresia.factura);
            $('#txtDesdeMembresia').val(desde[0]);
            $('#txtHastaMembresia').val(fecha[0]);

        }

    });

}

function consultarSaldosPendientes(idPersona) {

    

    htclibjs.Ajax({
        url: "/Biometrico/consultarSaldosPendientes",
        data: JSON.stringify({
            idPersona: idPersona
        }),

        success: function (r) {

            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th></th>' + //fila vacia donde va el boton de editar
                '<th>Concepto</th>' +
                '<th>Fecha</th>' +
                '<th>Valor</th>' +
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                var concepto = item.nombreProducto + ' ' + item.nombreBox;
                var fecha = item.fecha.split('T');
                trHTML +=

                    '<tr>' +
                    //Menu opciones de la tabla
                    '<td > ' +
                    '<div class="dropdown">' +
                    '<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                    '<i class="fas fa-cog"></i>' +
                    '</button>' +
                    '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +
                    '<a id ="' + item.id + '" class="dropdown-item btnSeleccionarSaldo" href="#"  >Pagar</a>' +
                    '</div>' +
                    '</div>' +
                    '</td > ' +
                    '<td >' + concepto + '</td>' +
                    '<td >' + fecha[0] + '</td>' +
                    '<td >' + item.valor + '</td>' +
                    '</tr> ';


            });

            trHTML +=
                '</tbody>';

            $('#tblSaldosPendientesBiometrico').html('');
            $('#tblSaldosPendientesBiometrico').append(trHTML);

            //accion al hacer clic en boton de la tabla
            $('.btnSeleccionarSaldo').on('click', function (e) {

                console.log('clic');
                selecionarSaldo($(this));

            });

        }

    });

}
function selecionarSaldo(boton) {

    var id = boton.attr("id");

    htclibjs.Ajax({
        url: "/Deuda/pagarDeuda",
        data: JSON.stringify({
            idDeuda: id
        }),
        success: function (r) {

            toastr.success("Deuda pagada", { positionClass: "toast-top-center" });
            consultarSaldosPendientes();
        }
    });

}