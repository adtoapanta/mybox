﻿$(document).ready(function () { initAtletas(); });

function initAtletas() {
    console.log('init Personas');

    $('#btnGuardarPersona').click(function (e) {
        guardarOEditar();
    });
    $('#btnGuardarMembresia').click(function (e) {
        nuevaMembresia();
    });

    $('#btnGuardarConsumo').click(function (e) {
        nuevoConsumo();
    });

    $('#modalAtletas').on('hidden.bs.modal', function () {
        limpiarCampos();
    });

    $('#modalMembresia').on('hidden.bs.modal', function () {
        limpiarCampos();
    });

    $('#modalConsumo').on('hidden.bs.modal', function () {
        limpiarCampos();
    });

    $('#txtProducto').change(function (e) {

        var idProducto = $('#txtProducto').val();
        if (idProducto != 0) {

            for (var i = 0; i < detalleProducto.length; i++) {
                if (idProducto == detalleProducto[i].idProducto)
                    $('#txtValorConsumo').val(detalleProducto[i].precioVenta);

            }
        }

    });


    exportTable();
    consultarAtletas();
    consultarProductos();
}

function nuevaMembresia() {

    console.log("Ingreso guardar Membresia");

    var idPersona = $("#txtIdPersona").val();
    var precio = $("#txtPrecioMembresia").val();
    var abono = $("#txtAbonoMembresia").val();
    var inicio = $("#txtDesdeMembresia").val();
    var diasRestantes = $("#txtTipoMembresia").val();
    var factura = $("#txtFacturaMembresia").val();
    var idFormaPago = $("#txtFormaPagoMembresia").val();
    var idTipoMembresia;
    if (diasRestantes == 12) {
        idTipoMembresia = 2;
    }
    else {
        idTipoMembresia = 1;
    }


    htclibjs.Ajax({
        url: "/Membresia/guardarMembresia",
        data: JSON.stringify({
            idPersona: idPersona, precio: precio, abono: abono, inicio: inicio, diasRestantes: diasRestantes, factura: factura, idFormaPago: idFormaPago, idTipoMembresia: idTipoMembresia
        }),
        success: function (r) {

            toastr.success("Membresia guardada", { positionClass: "toast-top-center" });
            limpiarCampos();
            $('#modalMembresia').modal('hide');
        }
    });


}

function consultarProductos() {

    htclibjs.Ajax({
        url: "/Producto/consultarProductosBox",
        data: JSON.stringify({

        }),
        success: function (r) {

            detalleProducto = r.data;

            $('#txtProducto').append(new Option('Seleccione', 0));

            $.each(r.data, function (i, item) {

                $('#txtProducto').append(new Option(item.Productos.nombre, item.idProducto));

            });

        }
    });

    limpiarCampos();
}

function nuevoConsumo() {

    var id = $("#txtIdPersona").val();
    var idProducto = $("#txtProducto").val();
    var valorConsumo = $("#txtValorConsumo").val();

    htclibjs.Ajax({
        url: "/Deuda/guardarDeuda",
        data: JSON.stringify({
            idPersona: id,
            valor: valorConsumo,
            idProducto: idProducto
        }),
        success: function (r) {

            toastr.success("Consumo agregado", { positionClass: "toast-top-center" });
            $('#modalConsumo').modal('hide');
            limpiarCampos();
        }
    });

}

function guardarOEditar() {

    var id = $("#txtIdPersona").val();

    if (id == 0) {
        guardarPersona();
    }
    else {
        editarPersona();
    }

    $('#modalAtletas').modal('hide');

}

function guardarPersona() {

    console.log("Ingreso guardar socio");
    var documento = $("#txtDocumento").val();
    var nombre1 = $("#txtNombre1").val();
    var nombre2 = $("#txtNombre2").val();
    var apellido1 = $("#txtApellido1").val();
    var apellido2 = $("#txtApellido2").val();
    var correo = $("#txtCorreo").val();
    var fechaNacimiento = $("#txtFechaNacimiento").val();
    var telefono = $("#txtTelefono").val();
    var celular = $("#txtCelular").val();

    htclibjs.Ajax({
        url: "/Persona/guardarPersona",
        data: JSON.stringify({
            documento: documento, nombre1: nombre1, nombre2: nombre2, apellido1: apellido1, apellido2: apellido2, correo: correo, fechaNacimiento: fechaNacimiento, telefono: telefono, celular: celular
        }),
        success: function (r) {

            toastr.success("Atleta guardado", { positionClass: "toast-top-center" });
            limpiarCampos();
            consultarAtletas();

        }
    });


}

function editarPersona() {

    console.log("Ingreso guardar socio");

    var id = $("#txtIdPersona").val();
    var documento = $("#txtDocumento").val();
    var nombre1 = $("#txtNombre1").val();
    var nombre2 = $("#txtNombre2").val();
    var apellido1 = $("#txtApellido1").val();
    var apellido2 = $("#txtApellido2").val();
    var correo = $("#txtCorreo").val();
    var fechaNacimiento = $("#txtFechaNacimiento").val();
    var telefono = $("#txtTelefono").val();
    var celular = $("#txtCelular").val();

    htclibjs.Ajax({
        url: "/Persona/guardarPersona",
        data: JSON.stringify({
            idPersona: id, documento: documento, nombre1: nombre1, nombre2: nombre2, apellido1: apellido1, apellido2: apellido2, correo: correo, fechaNacimiento: fechaNacimiento, telefono: telefono, celular: celular
        }),
        success: function (r) {

            toastr.success("Datos Actualizados", { positionClass: "toast-top-center" });
            limpiarCampos();
            consultarAtletas();
        }
    });


}

function consultarAtletas() {

    table.destroy();

    htclibjs.Ajax({
        url: "/Persona/consultarAtletas",
        data: JSON.stringify({
        }),
        success: function (r) {
            var url = SITEROOT;
            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th>...</th>' + //fila vacia donde va el boton de editar
                '<th>Nombre</th>' +
                '<th>Cédula</th>' +
                '<th>Correo</th>' +
                '<th>Celular</th>' +
                '<th>Teléfono</th>' +
                '<th>F. Nacimiento</th>' +
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                var fecha = item.fechaNacimiento.split('T');
                var nombreCompleto = item.nombre1 + ' ' + item.nombre2 + ' ' + item.apellido1 + ' ' + item.apellido2

                trHTML +=

                    '<tr>' +
                    //Menu opciones de la tabla
                    '<td id="tdIdPersona' + item.id + '"> ' +
                    '<div class="dropdown">' +
                    '<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                    '<i class="fas fa-cog"></i>' +
                    '</button>' +
                    '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +
                    '<a id ="' + item.id + '" class="dropdown-item btnSeleccionarPersona" href="#" data-toggle="modal" data-target="#modalAtletas" >Editar</a>' +
                    '<a id ="' + item.id + '" class="dropdown-item btnSeleccionarPersona" href="#" data-toggle="modal" data-target="#modalMembresia" >Nueva Membresia</a>' +
                    '<a id ="' + item.id + '" class="dropdown-item btnSeleccionarPersona" href="#" data-toggle="modal" data-target="#modalConsumo" >Nuevo Consumo</a>' +
                    '<a id ="' + item.id + '" class="dropdown-item" href="' + SITEROOT + '/Biometrico/Index/' + item.id + '"  >Consulta Biometrica</a>' +
                    '</div>' +
                    '</div>' +
                    '</td > ' +
                    '<td id="tdNombrePersona' + item.id + '" >' + nombreCompleto + '</td>' +
                    '<td id="tdDocumentoPersona' + item.id + '" >' + item.documento + '</td>' +
                    '<td id="tdCorreoPersona' + item.id + '">' + item.correo + '</td>' +
                    '<td id="tdCelularPersona' + item.id + '" ">' + item.celular + '</td>' +
                    '<td id="tdTelefonoPersona' + item.id + '" ">' + item.telefono + '</td>' +
                    '<td id="tdFechaNacimientoPersona' + item.id + '" >' + fecha[0] + '</td>' +

                    //'<td id="tdIdPersona' + item.id + '"> <button class="btnSeleccionarPersona btn btn-link  " value="' + item.id + '" type="button" data-toggle="modal" data-target="#modalAtletas" >Editar</button> </td>' + //boton editar de la tabla
                    '</tr> ';


            });

            trHTML +=
                '</tbody>';

            $('#tblAtletas').html('');
            $('#tblAtletas').append(trHTML);

            //accion al hacer clic en boton de la tabla
            $('.btnSeleccionarPersona').on('click', function (e) {

                console.log('clic');
                selecionarPersona($(this));

            });

            exportTable();
        }
    });

    limpiarCampos();
}

function selecionarPersona(boton) {

    var id = boton.attr("id");

    htclibjs.Ajax({
        url: "/Persona/consultarPersona",
        data: JSON.stringify({
            idPersona: id
        }),
        success: function (r) {

            var item = r.data;
            var fecha = item.fechaNacimiento.split('T');
            var nombreCompleto = item.nombre1 + ' ' + item.apellido1;

            //campos editar persona
            $("#txtIdPersona").val(item.id);
            $("#txtDocumento").val(item.documento);
            $("#txtNombre1").val(item.nombre1);
            $("#txtNombre2").val(item.nombre2);
            $("#txtApellido1").val(item.apellido1);
            $("#txtApellido2").val(item.apellido2);
            $("#txtCorreo").val(item.correo);
            $("#txtFechaNacimiento").val(fecha[0]);
            $("#txtTelefono").val(item.telefono);
            $("#txtCelular").val(item.celular);

            // campos nueva membresia
            $("#txtAtletaMembresia").val(nombreCompleto);

            // campos nuevo consumo
            $("#txtAtletaConsumo").val(nombreCompleto);

        }
    });

}

function exportTable() {

    table = $('#tblAtletas').DataTable({
        lengthChange: false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        language: SPANISHDATATABLE,

    });

    //table.buttons().container()
    //    .appendTo('#content-wrapper .col-md-6:eq(0)');

}

function limpiarCampos() {

    //campos editar persona

    $("#txtIdPersona").val(0);
    $("#txtDocumento").val('');
    $("#txtNombre1").val('');
    $("#txtNombre2").val('');
    $("#txtApellido1").val('');
    $("#txtApellido2").val('');
    $("#txtCorreo").val('');
    $("#txtFechaNacimiento").val('');
    $("#txtTelefono").val('');
    $("#txtCelular").val('');

    //campos nueva membresia
    $("#txtAtletaMembresia").val('');
    $("#txtPrecioMembresia").val('');
    $("#txtAbonoMembresia").val('');
    $("#txtDesdeMembresia").val('');
    $("#txtFacturaMembresia").val('');

    //campos nuevo consumo
    $("#txtAtletaConsumo").val('');
    $("#txtProducto").val(0);
    $("#txtValorConsumo").val('');

}

var table;

var detalleProducto;