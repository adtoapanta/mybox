﻿$(document).ready(function () { initOtroIngreso(); });

function initOtroIngreso() {
    console.log('init otroIngreso');

    $('#btnGuardarOtroIngreso').click(function (e) {
        guardarOEditar();

    });

    $('#btnConsultarOtrosIngresos').click(function (e) {
        consultarOtroIngresos();

    });


    //$('#btnGuardarInventario').click(function (e) {
    //    cerrarInventarioProducto();
    //});

    $('#modalOtrosIngresos').on('hidden.bs.modal', function () {
        limpiarCampos();
    });

    //$('#modalInventario').on('hidden.bs.modal', function () {
    //    limpiarCampos();
    //    consultarProductos();
    //});

    exportTable();
    //consultarProductos();
}

function guardarIngreso() {


    var concepto = $("#txtConcepto").val();
    var valor = $("#txtValor").val();
    var factura = $("#txtFactura").val();

    htclibjs.Ajax({
        url: "/OtroIngreso/guardarOtroIngreso",
        data: JSON.stringify({
            concepto: concepto,
            valor: valor,
            factura: factura
        }),
        success: function (r) {

            toastr.success("Ingreso Guardado", { positionClass: "toast-top-center" });            
            $("#modalOtrosIngresos").modal('hide');
            consultarOtroIngresos();
        }
    });

}

function guardarOEditar() {

    var id = $("#txtIdOtroIngreso").val();
    if (id == 0)
        guardarIngreso();
    else
        editarOtroIngreso();

}

function editarOtroIngreso() {

    var id = $("#txtIdOtroIngreso").val();
    var concepto = $("#txtConcepto").val();
    var valor = $("#txtValor").val();
    var factura = $("#txtFactura").val();

    htclibjs.Ajax({
        url: "/OtroIngreso/editarOtroIngreso",
        data: JSON.stringify({
            idOtroIngreso: id,
            concepto: concepto,
            valor: valor,
            factura: factura
        }),
        success: function (r) {

            toastr.success("Ingreso Actualizado", { positionClass: "toast-top-center" });
            $("#modalOtrosIngresos").modal('hide');
            consultarOtroIngresos();

        }
    });

}

function consultarOtroIngresos() {

    var desde = $("#txtDesde").val();
    var hasta = $("#txtHasta").val();

    table.destroy();

    htclibjs.Ajax({
        url: "/OtroIngreso/consultarOtrosIngresos",
        data: JSON.stringify({
            desde: desde,
            hasta: hasta
        }),
        success: function (r) {

            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th></th>' + //fila vacia donde va el boton de editar
                '<th>Concepto</th>' +
                '<th>Fecha</th>' +
                '<th>Factura</th>' +
                '<th>Valor</th>' +
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                var fecha = item.fecha.split('T');

                trHTML +=

                    '<tr>' +
                    //Menu opciones de la tabla
                    '<td id="tdIdPersona' + item.id + '"> ' +
                    '<div class="dropdown">' +
                    '<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                    '<i class="fas fa-cog"></i>' +
                    '</button>' +
                    '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +
                    '<a id ="' + item.id + '" class="dropdown-item btnSeleccionarIngreso" href="#" data-toggle="modal" data-target="#modalOtrosIngresos" >Editar</a>' +

                    '</div>' +
                    '</div>' +
                    '</td > ' +
                    '<td >' + item.concepto + '</td>' +
                    '<td >' + fecha[0] + '</td>' +
                    '<td >' + item.factura + '</td>' +
                    '<td >' + item.valor + '</td>' +
                    //'<td id="tdIdPersona' + item.id + '"> <button class="btnSeleccionarPersona btn btn-link  " value="' + item.id + '" type="button" data-toggle="modal" data-target="#modalAtletas" >Editar</button> </td>' + //boton editar de la tabla
                    '</tr> ';


            });

            trHTML +=
                '</tbody>';

            $('#tblOtrosIngresos').html('');
            $('#tblOtrosIngresos').append(trHTML);

            //accion al hacer clic en boton de la tabla
            $('.btnSeleccionarIngreso').on('click', function (e) {

                console.log('clic');
                selecionarOtroIngreso($(this));

            });

            exportTable();

        }
    });

}

function selecionarOtroIngreso(boton) {

    var id = boton.attr("id");

    htclibjs.Ajax({
        url: "/OtroIngreso/consultarOtroIngreso",
        data: JSON.stringify({
            idOtroIngreso: id
        }),
        success: function (r) {

            var item = r.data;

            $("#txtIdOtroIngreso").val(item.id);
            $("#txtConcepto").val(item.concepto);
            $("#txtValor").val(item.valor);
            $("#txtFactura").val(item.factura);

        }
    });

}

var table;

function exportTable() {

    table = $('#tblOtrosIngresos').DataTable({
        lengthChange: false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        language: SPANISHDATATABLE,

    });
        
}

function limpiarCampos() {

    $("#txtIdOtroIngreso").val(0);
    $("#txtConcepto").val('');
    $("#txtValor").val('');
    $("#txtFactura").val('');

}