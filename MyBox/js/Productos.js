﻿$(document).ready(function () { initProductos(); });

function initProductos() {
    console.log('init Productos');

    $('#btnGuardarProducto').click(function (e) {
        guardarOEditar();
    });

    $('#btnGuardarInventario').click(function (e) {
        cerrarInventarioProducto();
    });

    $('#modalProductos').on('hidden.bs.modal', function () {
        limpiarCampos();
    });

    $('#modalInventario').on('hidden.bs.modal', function () {
        limpiarCampos();
        consultarProductos();
    });

    exportTable();
    consultarProductos();
}

function cerrarInventarioProducto() {

    var id = $("#txtIdProducto").val();
    var inventarioFinal = $("#txtInventarioFinal").val();
    htclibjs.Ajax({
        url: "/Producto/cerrarInventarioProducto",
        data: JSON.stringify({
            idProducto: id,
            inventarioFinal: inventarioFinal
        }),
        success: function (r) {

            toastr.success("Inventario guardado", { positionClass: "toast-top-center" });
            $('#modalInventario').modal('hide');
            
        }
    });

}

function guardarOEditar() {

    var id = $("#txtIdProducto").val();

    if (id == 0) {
        guardarProducto();
    }
    else {
        editarProducto();
    }

    $('#modalProductos').modal('hide');

}

function consultarProductos() {

    table.destroy();

    htclibjs.Ajax({
        url: "/Producto/consultarProductosBox",
        data: JSON.stringify({

        }),
        success: function (r) {

            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th></th>' + //fila vacia donde va el boton de editar
                '<th>Nombre</th>' +
                '<th>Precio compra</th>' +
                '<th>Precio venta</th>' +
                '<th>stock</th>' +
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {


                trHTML +=

                    '<tr>' +
                    //Menu opciones de la tabla
                    '<td id="tdIdPersona' + item.id + '"> ' +
                    '<div class="dropdown">' +
                    '<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                    '<i class="fas fa-cog"></i>' +
                    '</button>' +
                    '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +
                    '<a id ="' + item.Productos.id + '" class="dropdown-item btnSeleccionarProducto" href="#" data-toggle="modal" data-target="#modalProductos" >Editar</a>' +
                    '<a id ="' + item.Productos.id + '" class="dropdown-item btnSeleccionarProducto" href="#" data-toggle="modal" data-target="#modalInventario" >Cerrar inventario</a>' +
                    '</div>' +
                    '</div>' +
                    '</td > ' +
                    '<td id="tdNombreProducto' + item.Productos.id + '" >' + item.Productos.nombre + '</td>' +
                    '<td id="tdPrecioCompraProducto' + item.Productos.id + '" >' + item.precioCompra + '</td>' +
                    '<td id="tdPrecioVentaProducto' + item.Productos.id + '">' + item.precioVenta + '</td>' +
                    '<td id="tdStockProducto' + item.Productos.id + '" ">' + item.stock + '</td>' +

                    //'<td id="tdIdPersona' + item.id + '"> <button class="btnSeleccionarPersona btn btn-link  " value="' + item.id + '" type="button" data-toggle="modal" data-target="#modalAtletas" >Editar</button> </td>' + //boton editar de la tabla
                    '</tr> ';


            });

            trHTML +=
                '</tbody>';

            $('#tblProductos').html('');
            $('#tblProductos').append(trHTML);

            //accion al hacer clic en boton de la tabla
            $('.btnSeleccionarProducto').on('click', function (e) {

                console.log('clic');
                selecionarProducto($(this));

            });

            exportTable();
        }
    });

    limpiarCampos();
}

function selecionarProducto(boton) {

    var id = boton.attr("id");

    htclibjs.Ajax({
        url: "/Producto/consultarProductoId",
        data: JSON.stringify({
            idProducto: id
        }),
        success: function (r) {

            var item = r.data;
            console.log(item);

            //campos editar producto
            $("#txtIdProducto").val(item.Productos.id);
            $("#txtNombreProducto").val(item.Productos.nombre);
            $("#txtStockProducto").val(item.stock);
            $("#txtPrecioCompra").val(item.precioCompra);
            $("#txtPrecioVenta").val(item.precioVenta);

            //campos cerrar inventario
            $("#txtProductoInventario").val(item.Productos.nombre);
            $("#txtInventarioInicial").val(item.stock);
            
        }
    });

}

function editarProducto() {

    console.log("Ingreso guardar socio");

    var id = $("#txtIdProducto").val();
    var nombreProducto = $("#txtNombreProducto").val();
    var stock = $("#txtStockProducto").val();
    var precioCompra = $("#txtPrecioCompra").val();
    var previoVenta = $("#txtPrecioVenta").val();


    htclibjs.Ajax({
        url: "/Producto/editarProducto",
        data: JSON.stringify({
            idProducto: id, nombreProducto: nombreProducto, stock: stock, precioCompra: precioCompra, precioVenta: previoVenta
        }),
        success: function (r) {

            toastr.success("Producto actualizado", { positionClass: "toast-top-center" });
            limpiarCampos();
            consultarProductos();

        }
    });

}

function guardarProducto() {

    console.log("Ingreso guardar socio");
    var nombreProducto = $("#txtNombreProducto").val();
    var stock = $("#txtStockProducto").val();
    var precioCompra = $("#txtPrecioCompra").val();
    var previoVenta = $("#txtPrecioVenta").val();


    htclibjs.Ajax({
        url: "/Producto/guardarProducto",
        data: JSON.stringify({
            nombreProducto: nombreProducto, stock: stock, precioCompra: precioCompra, precioVenta: previoVenta
        }),
        success: function (r) {

            toastr.success("Producto guardado", { positionClass: "toast-top-center" });
            limpiarCampos();
            consultarProductos();

        }
    });

}

function exportTable() {

    table = $('#tblProductos').DataTable({
        lengthChange: false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        language: SPANISHDATATABLE,

    });

    //table.buttons().container()
    //    .appendTo('#content-wrapper .col-md-6:eq(0)');

}

function limpiarCampos() {

    //campos editar producto

    $("#txtIdProducto").val(0);
    $("#txtNombreProducto").val('');
    $("#txtStockProducto").val('');
    $("#txtPrecioCompra").val('');
    $("#txtPrecioVenta").val('');

    //campos cerrar inventario
    $("#txtProductoInventario").val('');
    $("#txtInventarioInicial").val('');
    $("#txtInventarioFinal").val('');
}
var table;