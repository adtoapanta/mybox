﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBox.Models;
namespace MyBox.Extensions
{
    public class ProductoExtension
    {

        public static Productos guardarProducto(int idBox, string nombreProducto, int stock, decimal precioCompra, decimal precioVenta)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                Productos producto = new Productos();
                DetallesProducto detalle = new DetallesProducto();

                producto.nombre = nombreProducto;                

                a.Productos.Add(producto);
                a.SaveChanges();

                detalle.idProducto = producto.id;
                detalle.idBox = idBox;
                detalle.stock = stock;
                detalle.precioCompra = precioCompra;
                detalle.precioVenta = precioVenta;

                a.DetallesProducto.Add(detalle);
                a.SaveChanges();

                return producto;
            }
        }

        public static DetallesProducto editarProducto(int idProducto, int idBox, string nombreProducto, int stock, decimal precioCompra, decimal precioVenta)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                DetallesProducto detalle = a.DetallesProducto.Include("Productos").Where(x => x.idProducto == idProducto && x.idBox == idBox).FirstOrDefault();
                detalle.Productos.nombre = nombreProducto;
                detalle.stock = stock;
                detalle.precioCompra = precioCompra;
                detalle.precioVenta = precioVenta;
                
                a.SaveChanges();

                return detalle;
            }
        }

        public static DetallesProducto cerrarInventarioProducto(int idProducto, int idBox, int inventarioFinal, int idEmpleado)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                DetallesProducto detalle = a.DetallesProducto.Include("Productos").Where(x => x.idProducto == idProducto && x.idBox == idBox).FirstOrDefault();
                Inventarios inventario = new Inventarios();

                inventario.stockFinal = inventarioFinal;
                inventario.stockInicial = detalle.stock;
                inventario.fecha = DateTime.Now;
                inventario.idEmpleado = idEmpleado;
                inventario.idDetalleProducto = detalle.id;

                detalle.stock = inventarioFinal;

                a.Inventarios.Add(inventario);

                //crea otro ingreso
                string concepto = "venta" + detalle.Productos.nombre + " " + inventario.fecha;
                decimal valor = (decimal) (detalle.precioVenta * (inventario.stockInicial - inventario.stockFinal));
                OtrosIngresosExtension.guardarOtroIngreso(idBox, concepto, valor, "", 1);

                a.SaveChanges();

                return detalle;
            }
        }

        public static List<DetallesProducto> consultarProductosBox(int idBox)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                List<DetallesProducto> productos;

                productos = a.DetallesProducto.Include("Productos").Where(x => x.idBox == idBox).ToList();

                return productos;
            }
        }

        public static DetallesProducto consultarProductoId(int idProducto, int idBox)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                DetallesProducto producto;

                producto = a.DetallesProducto.Include("Productos").Where(x => x.idBox == idBox && x.idProducto == idProducto).FirstOrDefault();

                return producto;
            }
        }


    }
}