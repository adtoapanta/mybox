﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBox.Models;

namespace MyBox.Extensions
{
    public class PersonaExtension
    {
        public static List<Personas> consultarAtletas()
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                List<Personas> atletas = a.Personas.ToList();
                return atletas;
            }
        }

        public static Personas consultarPersona(int idPersona)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                Personas atleta = a.Personas.Where(x=> x.id == idPersona).FirstOrDefault();
                return atleta;
            }
        }

        public static Personas consultarBiometrico(int idPersona)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                Personas atleta = a.Personas.Include("Membresias").Where(x => x.id == idPersona || x.documento == idPersona.ToString()).FirstOrDefault();
                
                return atleta;
            }
        }

        
        public static Personas guardarPersona(string documento, string nombre1, string nombre2, string apellido1, string apellido2, string correo, DateTime fechaNacimiento, string telefono, string celular)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                Personas atleta = new Personas();

                atleta.documento = documento;
                atleta.nombre1 = nombre1;
                atleta.nombre2 = nombre2;
                atleta.apellido1 = apellido1;
                atleta.apellido2 = apellido2;
                atleta.correo = correo;
                atleta.fechaNacimiento = fechaNacimiento;
                atleta.telefono = telefono;
                atleta.celular = celular;

                a.Personas.Add(atleta);
                a.SaveChanges();

                return atleta;
            }
        }

        public static void elimintarAtleta(int idPersona)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                Personas atleta = a.Personas.Where(x => x.id == idPersona).First();

                a.Personas.Remove(atleta);                
                a.SaveChanges();

            }
        }

        public static Personas editarPersona(int idPersona, string documento, string nombre1, string nombre2, string apellido1, string apellido2, string correo, DateTime fechaNacimiento, string telefono, string celular)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                Personas atleta = a.Personas.Where(x => x.id == idPersona).First();

                atleta.documento = documento;
                atleta.nombre1 = nombre1;
                atleta.nombre2 = nombre2;
                atleta.apellido1 = apellido1;
                atleta.apellido2 = apellido2;
                atleta.correo = correo;
                atleta.fechaNacimiento = fechaNacimiento;
                atleta.telefono = telefono;
                atleta.celular = celular;
                                
                a.SaveChanges();

                return atleta;
            }
        }
    }
}