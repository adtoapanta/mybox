﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBox.Models;
using System.Data.SqlClient;
namespace MyBox.Extensions
{
    public class DeudasExtension
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public DateTime fecha { get; set; }
        public decimal valor { get; set; }
        public string nombreProducto { get; set; }
        public string nombreBox { get; set; }

        public static Deudas guardarDeuda(int idPersona, decimal valor, int idEmpleado, int idBox, int idProducto)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                Deudas deuda = new Deudas();

                deuda.idPersona = idPersona;
                deuda.fecha = DateTime.Now;
                deuda.valor = valor;
                deuda.idEmpleado = idEmpleado;
                deuda.idBox = idBox;
                deuda.idProducto = idProducto;
                deuda.pagado = false;

                a.Deudas.Add(deuda);
                a.SaveChanges();

                return deuda;
            }
        }

        

        public static List<DeudasExtension> consultarSaldosPendientes(int idPersona, int idBox)
        {
            using (var a = new Models.MyBoxEntities())
            {
                return a.Database.SqlQuery<DeudasExtension>("SP_ConsultaSaldosPendientes @idPersona", new SqlParameter("idPersona", idPersona)).ToList();
            }
        }
        public static void pagarDeuda(int idDeuda)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                Deudas deuda = a.Deudas.Where(x => x.id == idDeuda).FirstOrDefault();

                deuda.pagado = true;

                a.SaveChanges();
                                
            }
        }
        public static Deudas consultarDeuda(int idDeuda)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                Deudas deuda = a.Deudas.Where(x => x.id == idDeuda).FirstOrDefault();

                return deuda;
            }
        }
    }

    
}