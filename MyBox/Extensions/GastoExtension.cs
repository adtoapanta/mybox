﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBox.Models;

namespace MyBox.Extensions
{
    public class GastoExtension
    {

        public static Gastos guardarGasto(int idBox, string proveedor, string concepto, decimal valor, string factura, int idEmpleado)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                Gastos gasto = new Gastos();

                gasto.idBox = idBox;
                gasto.fecha = DateTime.Now;
                gasto.proveedor = proveedor;
                gasto.concepto = concepto;
                gasto.valor = valor;
                gasto.factura = factura;
                gasto.idEmpleado = idEmpleado;

                a.Gastos.Add(gasto);
                a.SaveChanges();

                return gasto;
            }
        }

        public static Gastos editarGasto(int idGasto, string proveedor, string concepto, decimal valor, string factura, int idEmpleado)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                Gastos gasto = a.Gastos.Where(x => x.id == idGasto).FirstOrDefault();
                
                gasto.proveedor = proveedor;
                gasto.concepto = concepto;
                gasto.valor = valor;
                gasto.factura = factura;
                gasto.idEmpleado = idEmpleado;
                
                a.SaveChanges();

                return gasto;
            }
        }

        public static List<Gastos> consultarGasto(int idBox, DateTime desde, DateTime hasta)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                List<Gastos> gastos = a.Gastos.Where(x => x.idBox == idBox && x.fecha >= desde && x.fecha <= hasta).ToList();

                return gastos;
            }
        }

        public static Gastos consultarGasto(int idGasto)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                Gastos gasto = a.Gastos.Where(x => x.id == idGasto).FirstOrDefault();

                return gasto;
            }
        }
    }
}