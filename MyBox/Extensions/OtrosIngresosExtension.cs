﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBox.Models;

namespace MyBox.Extensions
{
    public class OtrosIngresosExtension
    {

        public static OtrosIngresos guardarOtroIngreso(int idBox, string concepto, decimal valor, string factura, int idEmpleado)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                OtrosIngresos otro = new OtrosIngresos();

                otro.idBox = idBox;
                otro.fecha = DateTime.Now;
                otro.concepto = concepto;
                otro.valor = valor;
                otro.factura = factura;
                otro.idEmpleado = idEmpleado;

                a.OtrosIngresos.Add(otro);
                a.SaveChanges();

                return otro;
            }
        }

        public static OtrosIngresos editarOtroIngreso(int idOtroIngreso, string concepto, decimal valor, string factura, int idEmpleado)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                OtrosIngresos otro = a.OtrosIngresos.Where(x => x.id == idOtroIngreso).FirstOrDefault();                               
                
                otro.concepto = concepto;
                otro.valor = valor;
                otro.factura = factura;
                otro.idEmpleado = idEmpleado;
                
                a.SaveChanges();

                return otro;
            }
        }

        public static List<OtrosIngresos> consultarOtrosIngresos(int idBox, DateTime desde, DateTime hasta)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                List<OtrosIngresos> otros = a.OtrosIngresos.Where(x => x.idBox == idBox && x.fecha >= desde && x.fecha <= hasta).ToList();                

                return otros;
            }
        }

        public static OtrosIngresos consultarOtroIngreso(int idOtroIngreso)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                OtrosIngresos otro = a.OtrosIngresos.Where(x => x.id == idOtroIngreso).FirstOrDefault();

                return otro;
            }
        }

    }
}