﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBox.Models;
namespace MyBox.Extensions
{
    public class MembresiaExtension
    {
        public static Membresias guardarMembresia(int idBox, int idPersona, decimal precio, decimal abono, DateTime inicio, 
            int diasRestantes, string factura, int idFormaPago, int idTipoMembresia, int idEmpleado)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                Membresias membresia = new Membresias();

                membresia.idBox = idBox;
                membresia.idPersona = idPersona;
                membresia.fecha = DateTime.Now;
                membresia.precio = precio;
                membresia.abono = abono;
                membresia.inicio = inicio;
                membresia.fin = inicio.AddDays(diasRestantes);
                membresia.diasRestantes = diasRestantes;
                membresia.factura = factura;
                membresia.idFormaPago = idFormaPago;
                membresia.idTipoMembresia = idTipoMembresia;
                membresia.notificado = false;
                membresia.renovado = false;
                membresia.idEmpleado = idEmpleado;
                membresia.activo = true;

                if (abono < precio)
                {
                    DeudasExtension.guardarDeuda(idPersona, (precio - abono), 1, 1,1);
                }

                a.Membresias.Add(membresia);
                a.SaveChanges();

                return membresia;
            }
        }

        public static void editarMembresia(int idMembresia, int diasExtra, string factura)
        {
            using (var a = new Models.MyBoxEntities())
            {
                Membresias membresia = a.Membresias.Where(x => x.id == idMembresia).FirstOrDefault();
                DateTime hasta = (DateTime)membresia.fin;

                membresia.fin = hasta.AddDays(diasExtra);
                membresia.factura = factura;

                a.SaveChanges();
            }
        }

        public static Membresias consultarMembresia(int idMembresia)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                Membresias membresia = a.Membresias.Where(x => x.id == idMembresia).FirstOrDefault();

                return membresia;                
            }
        }
    }
}