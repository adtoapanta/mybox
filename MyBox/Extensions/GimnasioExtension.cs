﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBox.Models;
namespace MyBox.Extensions
{
    public class GimnasioExtension
    {
        public static List<Gimnasios> consultarGimnasios()
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                List<Gimnasios> gimnasios = a.Gimnasios.ToList();

                return gimnasios;
            }
        }
    }
}