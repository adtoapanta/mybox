﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBox.Models;
namespace MyBox.Extensions
{
    public class GastosExtension
    {
        public static Gastos guardarGasto(int idBox, DateTime fecha, string proveedor, string concepto, decimal valor, string factura, int idEmpleado)
        {
            using (var a = new Models.MyBoxEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                Gastos gasto = new Gastos();

                gasto.idBox = idBox;
                gasto.fecha = DateTime.Now;
                gasto.proveedor = proveedor;
                gasto.concepto = concepto;
                gasto.valor = valor;
                gasto.factura = factura;
                gasto.idEmpleado = idEmpleado;

                a.Gastos.Add(gasto);
                a.SaveChanges();

                return gasto;
            }
        }
    }
}