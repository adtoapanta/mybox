﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyBox.Extensions
{
    public class Resultado
    {

        public Boolean exitoso { get; set; }
        public string data;
        public int codigo { get; set; }

        public Resultado()
        {
        }

        public Resultado(Boolean exitoso, object data, int codigo = 0)
        {
            this.exitoso = exitoso;
            //Prefiero serializar con newtonsoftjson
            this.data = JsonConvert.SerializeObject(data, new JsonSerializerSettings()
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                Formatting = Formatting.Indented
            });
            this.codigo = codigo;
        }




    }
}