﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyBox.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
namespace MyBox.Controllers
{
    public class PersonaController : Controller
    {
        // GET: Persona
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Atletas()
        {
            return View();
        }

        public JsonResult guardarPersona(string documento, string nombre1, string nombre2, string apellido1, string apellido2, string correo, DateTime fechaNacimiento, string telefono, string celular)
        {
            Resultado resultado;
            try
            {
                

                resultado = new Resultado(true, PersonaExtension.guardarPersona(documento,nombre1,nombre2,apellido1,apellido2,correo,fechaNacimiento,telefono,celular)); 
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
        public JsonResult consultarAtletas()
        {
            Resultado resultado;
            try
            {


                resultado = new Resultado(true, PersonaExtension.consultarAtletas());
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult consultarPersona(int idPersona)
        {
            Resultado resultado;
            try
            {
                
                resultado = new Resultado(true, PersonaExtension.consultarPersona(idPersona));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult editarPersona(int idPersona, string documento, string nombre1, string nombre2, string apellido1, string apellido2, string correo, DateTime fechaNacimiento, string telefono, string celular)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, PersonaExtension.editarPersona(idPersona,documento,nombre1,nombre2,apellido1,apellido2,correo,fechaNacimiento,telefono,celular));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

    }

    
}