﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyBox.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace MyBox.Controllers
{
    public class OtroIngresoController : Controller
    {
        // GET: OtroIngreso
        public ActionResult Todos()
        {
            return View();
        }

        public JsonResult guardarOtroIngreso(string concepto, decimal valor, string factura)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, OtrosIngresosExtension.guardarOtroIngreso(1,concepto,valor,factura,1));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult editarOtroIngreso(int idOtroIngreso, string concepto, decimal valor, string factura)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, OtrosIngresosExtension.editarOtroIngreso(idOtroIngreso,concepto,valor,factura,1));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult consultarOtrosIngresos( DateTime desde, DateTime hasta)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, OtrosIngresosExtension.consultarOtrosIngresos(1,desde,hasta));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult consultarOtroIngreso(int idOtroIngreso)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, OtrosIngresosExtension.consultarOtroIngreso(idOtroIngreso));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
    }
}