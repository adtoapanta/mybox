﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyBox.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace MyBox.Controllers
{
    public class DeudaController : Controller
    {
        // GET: Deuda
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult guardarDeuda(int idPersona, decimal valor, int idProducto)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, DeudasExtension.guardarDeuda(idPersona,valor,1,1,idProducto));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult pagarDeuda(int idDeuda)
        {
            Resultado resultado;
            try
            {
                DeudasExtension.pagarDeuda(idDeuda);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

    }

    
}