﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyBox.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace MyBox.Controllers
{
    public class GastoController : Controller
    {
        // GET: Gasto
        public ActionResult Todos()
        {
            return View();
        }

        public JsonResult guardarGasto(string proveedor, string concepto, decimal valor, string factura)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, GastoExtension.guardarGasto(1,proveedor,concepto,valor,factura, 1));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult editarGasto(int idGasto, string proveedor, string concepto, decimal valor, string factura)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, GastoExtension.editarGasto(idGasto, proveedor, concepto, valor, factura, 1));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult consultarGastos(DateTime desde, DateTime hasta)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, GastoExtension.consultarGasto(1,desde,hasta));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult consultarGasto(int idGasto)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, GastoExtension.consultarGasto(idGasto));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

    }
}