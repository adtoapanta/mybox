﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyBox.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
namespace MyBox.Controllers
{
    public class MembresiaController : Controller
    {
        // GET: Membresia
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Nueva()
        {
            return View();
        }

        public JsonResult guardarMembresia(int idPersona, decimal precio, decimal abono, DateTime inicio, 
            int diasRestantes, string factura, int idFormaPago, int idTipoMembresia)
        {
            Resultado resultado;
            try
            {
                
                resultado = new Resultado(true, MembresiaExtension.guardarMembresia(1,idPersona,precio,abono,inicio,diasRestantes,factura,idFormaPago,idTipoMembresia,1));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult consultarMembresia(int idMembresia)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, MembresiaExtension.consultarMembresia(idMembresia));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult editarMembresia(int idMembresia, int diasExtra, string factura)
        {
            Resultado resultado;
            try
            {
                MembresiaExtension.editarMembresia(idMembresia, diasExtra, factura);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
    }
}
