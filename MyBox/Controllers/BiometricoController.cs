﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyBox.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace MyBox.Controllers
{
    public class BiometricoController : Controller
    {
        // GET: Biometrico
        public ActionResult Index()
        {
            if (Url.RequestContext.RouteData.Values["id"] != null)

                ViewBag.idPersona = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());

            else
                ViewBag.idPersona = "";          


            return View();            
        }

        public JsonResult consultarBiometrico(int idPersona)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, PersonaExtension.consultarBiometrico(idPersona));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult consultarSaldosPendientes(int idPersona)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, DeudasExtension.consultarSaldosPendientes(idPersona,1));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
    }
}