﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyBox.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace MyBox.Controllers
{
    public class ProductoController : Controller
    {
        // GET: Producto
        public ActionResult Editar()
        {
            return View();
        }

        public JsonResult guardarProducto( string nombreProducto, int stock, decimal precioCompra, decimal precioVenta)
        {
            Resultado resultado;
            try
            {
                
                resultado = new Resultado(true, ProductoExtension.guardarProducto(1,nombreProducto,stock,precioCompra,precioVenta));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult editarProducto(int idProducto, string nombreProducto, int stock, decimal precioCompra, decimal precioVenta)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, ProductoExtension.editarProducto(idProducto,1, nombreProducto, stock, precioCompra, precioVenta));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult consultarProductosBox()
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, ProductoExtension.consultarProductosBox(1));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult consultarProductoId(int idProducto)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, ProductoExtension.consultarProductoId(idProducto,1));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult cerrarInventarioProducto(int idProducto, int inventarioFinal)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, ProductoExtension.cerrarInventarioProducto(idProducto,1,inventarioFinal,1));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
    }
}